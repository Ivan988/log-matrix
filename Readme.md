
Set up project:
* make sure you have docker installed and started
* docker-compose up
* install dependencies (docker exec -it logmatrix_php_1 composer install)
* start command to import data from log file (docker exec -it logmatrix_php_1 php bin/console import:logs)
* example of get request: "http://localhost:8080/matrix?fromDateTime=2017-10-10&toDateTime=2019-10-17&statusCode=201&service=USER-SERVICE"
* fromDateTime and toDateTime can be any strtotime valid format