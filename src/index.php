<?php

require_once __DIR__.'/vendor/autoload.php';

use Mongo\LogRepository;
use Adapters\MatrixQueryRequest;
use Symfony\Component\HttpFoundation\Request;

$app = new Silex\Application();

$app->get('/matrix', function (Request $request) use ($app) {

    $requestAdapter = new MatrixQueryRequest($request);
    $args = $requestAdapter->getFormatedRequest();

    $logRepository = new LogRepository();
    $result = $logRepository->countLogsBy($args);

    return $app->json([
        'count' => $result,
    ]);
});

$app->run();
