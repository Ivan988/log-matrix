<?php

namespace Parsers;

use MongoDB\BSON\UTCDatetime;

class LogParser
{
    public static function parse($logEntry)
    {
        $logEntry = trim($logEntry);

        return [
            'service' => self::getService($logEntry),
            'statusCode' => self::getStatusCode($logEntry),
            'dateTime' => self::getDateTime($logEntry),
        ];
    }

    protected static function getService($logEntry)
    {
        return trim(explode('- -', $logEntry)[0]);
    }

    protected static function getStatusCode($logEntry)
    {
        return substr($logEntry, strrpos($logEntry, ' ') + 1);
    }

    protected static function getDateTime($logEntry)
    {
        $from = strpos($logEntry, '[');
        $to = strpos($logEntry, ']');
        $dateTimeStr = substr($logEntry , $from + 1, $to - $from - 1);
        $dateTime = strtotime($dateTimeStr);
        return $dateTime ? new UTCDatetime($dateTime * 1000) : NULL;
    }
}
