<?php

namespace Adapters;

use MongoDB\BSON\UTCDatetime;
use Symfony\Component\HttpFoundation\Request;

class MatrixQueryRequest
{
    /**
     * @var Request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getFormatedRequest()
    {
        $service = $this->request->get('service');
        $statusCode = $this->request->get('statusCode');
        $fromDateTime = $this->request->get('fromDateTime');
        $toDateTime = $this->request->get('toDateTime');

        $args = [];
        if ($service) {
            $args['service'] = $service;
        }
        if ($statusCode) {
            $args['statusCode'] = $statusCode;
        }
        if ($fromDateTime || $toDateTime) {
            $args['dateTime'] = [];
        }
        if ($fromDateTime) {
            $args['dateTime']['$gte'] = $this->formatDate($fromDateTime);
        }
        if ($toDateTime) {
            $args['dateTime']['$lte'] = $this->formatDate($toDateTime);
        }

        return $args;
    }

    protected function formatDate($dateFromRequest)
    {
        return new UTCDatetime(strtotime($dateFromRequest) * 1000);
    }
}
