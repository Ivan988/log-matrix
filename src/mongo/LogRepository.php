<?php

namespace Mongo;

class LogRepository
{
    public function countLogsBy($args)
    {
        $client = Connection::get();

        return $client->logs->serverLogs->countDocuments($args);
    }
}
