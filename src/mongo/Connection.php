<?php

namespace Mongo;

use MongoDB\Client;

class Connection
{
    /** @var Client */
    protected static $connection;

    public static function get()
    {
        if (!self::$connection) {
            self::$connection = new Client("mongodb://root:example@mongo:27017");
        }
        return self::$connection;
    }
}
