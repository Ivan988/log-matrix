<?php

namespace Mongo;

class LogObjectManager
{
    public function saveDocument($document)
    {
        $client = Connection::get();
        $database = $client->selectDatabase('logs');
        $collection = $database->selectCollection('serverLogs');

        if (!$collection) {
            $collection = $database->createCollection('serverLogs');
        }

        return $collection->insertOne($document);
    }
}
