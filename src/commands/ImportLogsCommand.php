<?php

namespace Commands;

use Mongo\LogObjectManager;
use Parsers\LogParser;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportLogsCommand extends Command
{
    /**
     * @var string
     */
    protected $logPath = '/var/www/html/resources/logs.log';

    protected function configure()
    {
        $this->setName('import:logs');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Importing data from log file started...');

        $handle = fopen($this->logPath, 'r');
        $objectManager = new LogObjectManager();

        if ($handle) {
            $line = fgets($handle);
            while ($line !== false) {
                $objectManager->saveDocument(LogParser::parse($line));
                $output->writeln('Parsing line:' . $line);
                $line = fgets($handle);
            }
        }

        $output->writeln('Importing data from log file success...');
    }
}